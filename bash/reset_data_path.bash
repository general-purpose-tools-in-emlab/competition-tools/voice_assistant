#!/bin/bash

rm -r ../data/*
rm -r ../src/*.wav
rm -r ../src/run.py
rm -r ../src/voicevox_core-0.14.3+cpu-cp38-abi3-linux_x86_64.whl
rm -r ../src/open_jtalk_dic_utf_8-1.11.tar.gz
rm -r ../src/onnxruntime-linux-x64-1.13.1.tgz
rm -r ../src/libonnxruntime.so.1.13.1
rm -r ../src/open_jtalk_dic_utf_8-1.11
rm -r ../src/onnxruntime-linux-x64-1.13.1

mkdir -p ../data/
