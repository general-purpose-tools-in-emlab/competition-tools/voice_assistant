#!/bin/bash

cd ../src/

# Setup python's binding of voicevox core
wget https://github.com/VOICEVOX/voicevox_core/releases/download/0.14.3/voicevox_core-0.14.3+cpu-cp38-abi3-linux_x86_64.whl

# Download ONNX Runtime
wget https://github.com/microsoft/onnxruntime/releases/download/v1.13.1/onnxruntime-linux-x64-1.13.1.tgz
tar xvzf onnxruntime-linux-x64-1.13.1.tgz
mv onnxruntime-linux-x64-1.13.1/lib/libonnxruntime.so.1.13.1 ./

# Download Open Jtalk dictionary
wget http://downloads.sourceforge.net/open-jtalk/open_jtalk_dic_utf_8-1.11.tar.gz
tar xvzf open_jtalk_dic_utf_8-1.11.tar.gz

# Download sample program
wget https://raw.githubusercontent.com/VOICEVOX/voicevox_core/406f6c41408836840b9a38489d0f670fb960f412/example/python/run.py
