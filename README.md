# 'voice_assistant' Package

*   Maintainer: Shoichi Hasegawa ([hasegawa.shoichi@em.ci.ritsumei.ac.jp](mailto:hasegawa.shoichi@em.ci.ritsumei.ac.jp)).
*   Author: Shoichi Hasegawa ([hasegawa.shoichi@em.ci.ritsumei.ac.jp](mailto:hasegawa.shoichi@em.ci.ritsumei.ac.jp)).

**Content:**
*   [Setup](#Setup)
*   [Launch](#launch)
*   [Files](#files)

## Setup (Confirmed)
```
Ubuntu: 20.04LTS  
Python: 3.8.10  
```
1. Please install these packages.   
- `pip install -r requirements.txt`  
- `apt install -y ffmpeg`  
- `apt install -y pulseaudio`  
- `apt install -y portaudio19-dev`  
- `apt install -y lame`

2. Please setup about VOICEVOX
- Move /voice_assistant/bash  
- `bash setup_voicevox.bash`  
- Move /voice_assistant/src
- `python run.py --mode AUTO --dict-dir ./open_jtalk_dic_utf_8-1.11 --text "こんにちは、ずんだもんなのだ。" --out ./output1.wav --speaker-id 26`  

## Launch
- `python chatbot_gpt.py`

## Files
- `chatbot_gpt.py`: Terminalにテキスト入力すると、GPT-4が応答文を生成し、音声合成する (VOICEVOX or Google Text to Speech).  
- `chatbot_gpt_whisper.py`: マイクロフォンで入力した音声をwhisperが認識し、認識結果からGPT-4が応答文を生成し、音声合成する (VOICEVOX or Google Text to Speech).  


## References

