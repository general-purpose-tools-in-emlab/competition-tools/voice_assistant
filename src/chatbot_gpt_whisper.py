#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import openai
from pathlib import Path
import voicevox_core
from voicevox_core import AccelerationMode, AudioQuery, VoicevoxCore
import sounddevice as sd
import numpy as np
import wave
from pydub import AudioSegment
from gtts import gTTS
import tempfile
import simpleaudio as sa
from pathlib import Path
import os
from io import BytesIO
import threading
import time
import yaml
import pyaudio
import sys

config_path = "../config/config.yaml"
output_path = Path("../data/output.wav")
with open(config_path, 'r') as f:
    config = yaml.safe_load(f)
SPEAKER_ID = config['SPEAKER_ID']
OPEN_JTALK_DIC_UTF_8 = config['OPEN_JTALK_DIC_UTF_8']
RATE = config['RATE']

converted_path = '../data/converted_path' # 107
acceleration_mode = AccelerationMode.AUTO
CORE = VoicevoxCore(
    acceleration_mode=acceleration_mode, open_jtalk_dict_dir=OPEN_JTALK_DIC_UTF_8
)
CORE.load_model(SPEAKER_ID)
sd.default.device = 13

def generate_text(prompt, conversation_history):
    # ユーザーの質問を会話履歴に追加
    conversation_history.append({"role": "user", "content": prompt})

    # GPT-4を使用してテキストを生成
    response = openai.ChatCompletion.create(
        model="gpt-4",
        messages=[{"role": "system", "content": "You are an excellent household robot."}] + conversation_history,
        max_tokens=2048,
        n=1,
        temperature=0.0,
    )
    message = response.choices[0].message['content'].strip()

    # GPT-4の回答を会話履歴に追加
    conversation_history.append({"role": "assistant", "content": message})

    return message

def sound_synthesis_voicebox(text): 
    audio_query = CORE.audio_query(text, SPEAKER_ID)
    wav = CORE.synthesis(audio_query, SPEAKER_ID)

    if not os.path.exists("../data/"):
        os.makedirs("../data/")

    output_path.write_bytes(wav)
    converted_path = convert_samplerate(output_path)
    play_sound_with_sd(converted_path)

def sound_synthesis_gtts(text, select_lang):
    speech = gTTS(text=text, lang=select_lang, slow=False) # ja, en
    temp_file = tempfile.NamedTemporaryFile(delete=False, suffix=".mp3")
    speech.save(temp_file.name)
    audio = AudioSegment.from_mp3(temp_file.name)
    audio = audio.set_channels(1)
    wav_data = audio.export(format="wav")

    if not os.path.exists("../data/"):
        os.makedirs("../data/")
        
    audio.export(output_path, format="wav")
    converted_path = convert_samplerate(output_path)
    play_sound_with_sd(converted_path)
    os.remove(temp_file.name)

def play_sound_with_sd(file_path):
    with wave.open(str(file_path), 'r') as wf:
        samplerate = wf.getframerate()
        # print(f"sounddevice_wav: {samplerate}")
        data = wf.readframes(wf.getnframes())
        samples = np.frombuffer(data, dtype=np.int16)
        sd.play(samples, samplerate)
        sd.wait()

# def load_prompt(file_path):
#     """
#     Load the prompt from the file.
#     """
#     with open(file_path, "r") as f:
#         prompt = f.read()

#     return prompt

def convert_samplerate(file_path):
    sound = AudioSegment.from_wav(file_path)
    sound = sound.set_frame_rate(RATE)
    new_file_path = converted_path
    sound.export(converted_path, format="wav")
    return converted_path

# マルチスレッド処理
def async_run(func, *args):
    thread = threading.Thread(target=func, args=args)
    thread.start()
    return thread

def load_api_key(file_path):
    """
    Load the API key from the file.
    """

    with open(file_path, "r") as f:
        openai.api_key = f.read().strip()

def record(seconds, filename="audio.wav", save_as_mp3=False):
    # 基本的なパラメータの設定
    CHUNK = 1024
    FORMAT = pyaudio.paInt16
    CHANNELS = 1
    RATE = 16000

    p = pyaudio.PyAudio()

    stream = p.open(format=FORMAT,
                    channels=CHANNELS,
                    rate=RATE,
                    input=True,
                    frames_per_buffer=CHUNK,
                    input_device_index=13)

    print("Recording...")

    frames = []

    for _ in range(0, int(RATE / CHUNK * seconds)):
        data = stream.read(CHUNK)
        frames.append(data)

    print("Finished recording.")

    stream.stop_stream()
    stream.close()
    p.terminate()

    with wave.open(filename, 'wb') as wf:
        wf.setnchannels(CHANNELS)
        wf.setsampwidth(p.get_sample_size(FORMAT))
        wf.setframerate(RATE)
        wf.writeframes(b"".join(frames))

    if save_as_mp3:
        audio = AudioSegment.from_wav(filename)
        audio.export(filename.replace(".wav", ".mp3"), format="mp3")
        # 必要に応じて、WAVファイルを削除
        # os.remove(filename)


if __name__ == "__main__":

    load_api_key("../access_keys/OPENAI_API_KEY.key")
    conversation_history = []
    # prompt = self.load_prompt("../data/prompt.txt")
    # system = {"role": "system", "content": prompt}
    # conversation_history.append(system)

    while True:
        input_data = input(f"モデルを選択してください. Google TTS (en): 1, Google TTS (ja): 2, VOICEVOX: 3: ")

        if input_data.lower() == '1' or '2' or '3':
            break
        print(f"もう一度入力してください。")
    
    if str(input_data.lower()) == '1': # Google TTS (en)
        while True:
            input_prompt = input(f"Enterキーを入力してください（終了するには'q'を入力）: ")
            
            if input_prompt.lower() == 'q':
                break
            
            elif input_prompt == '':
                # 使用例
                print(f"喋りかけてください (制限時間：10秒)")
                record(10, "/root/HSR/catkin_ws/src/voice_assistant/data/test.mp3", save_as_mp3=True)
                text = generate_text("Hello.", conversation_history)
                print(f"GPT-4からの回答: {text}")
                # sound_synthesis_gtts(text)
                async_run(sound_synthesis_gtts, text, "en")
            else:
                pass

    elif str(input_data.lower()) == '2': # Google TTS (ja)
        while True:
            input_prompt = input(f"質問を入力してください（終了するには'q'を入力）: ")

            if input_prompt.lower() == 'q':
                break

            text = generate_text(input_prompt, conversation_history)
            print(f"GPT-4からの回答: {text}")
            # sound_synthesis_gtts(text)
            async_run(sound_synthesis_gtts, text, "ja")

    elif str(input_data.lower()) == '3': # VOICEVOX
        while True:
            input_prompt = input(f"質問を入力してください（終了するには'q'を入力）: ")

            if input_prompt.lower() == 'q':
                break

            text = generate_text(input_prompt, conversation_history)
            print(f"GPT-4からの回答: {text}")
            # sound_synthesis_voicebox(text)
            async_run(sound_synthesis_voicebox, text)
