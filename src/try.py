#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import openai
openai.api_key = ""

from pathlib import Path
import voicevox_core
from voicevox_core import AccelerationMode, AudioQuery, VoicevoxCore
import sounddevice as sd
import numpy as np
import wave
from pydub import AudioSegment
from gtts import gTTS
import tempfile
import simpleaudio as sa
from pathlib import Path
import os
from io import BytesIO
import threading
import time

SPEAKER_ID = 2
open_jtalk_dict_dir = '../voicebox_config/open_jtalk_dic_utf_8-1.11'
out = Path('output2.wav')
acceleration_mode = AccelerationMode.AUTO
CORE = VoicevoxCore(
    acceleration_mode=acceleration_mode, open_jtalk_dict_dir=open_jtalk_dict_dir
)
CORE.load_model(SPEAKER_ID)
# sd.default.device = 13

def generate_text(prompt, conversation_history):
    # ユーザーの質問を会話履歴に追加
    conversation_history.append({"role": "user", "content": prompt})

    # GPT-4モデルを使用してテキストを生成
    response = openai.ChatCompletion.create(
        model="gpt-4",
        messages=[{"role": "system", "content": "You are an excellent household robot."}] + conversation_history,
        max_tokens=2048,
        n=1,
        temperature=0.0,
    )
    message = response.choices[0].message['content'].strip()

    # アシスタントの回答を会話履歴に追加
    conversation_history.append({"role": "assistant", "content": message})

    return message

def sound_synthesis_voicebox(text): 
    audio_query = CORE.audio_query(text, SPEAKER_ID)
    wav = CORE.synthesis(audio_query, SPEAKER_ID)
    out.write_bytes(wav)
    converted_path = convert_samplerate(out)
    play_sound_with_sd(converted_path)

def sound_synthesis_gtts(text): # 英語用
    speech = gTTS(text=text, lang='en', slow=False) # ja, en
    temp_file = tempfile.NamedTemporaryFile(delete=False, suffix=".mp3")
    speech.save(temp_file.name)
    audio = AudioSegment.from_mp3(temp_file.name)
    audio = audio.set_channels(1)  # simpleaudioがモノラルのみサポートしているため
    
    # WAV形式のバイトデータに変換
    wav_data = audio.export(format="wav")

    out_path = "./output.wav"
    audio.export(out_path, format="wav")
    converted_path = convert_samplerate(Path(out_path))
    play_sound_with_sd(converted_path)
    # 一時ファイルを削除
    os.remove(temp_file.name)


def play_sound_with_sd(file_path):
    with wave.open(str(file_path), 'r') as wf:
        samplerate = wf.getframerate()
        # print(f"sounddevice_wav: {samplerate}")
        data = wf.readframes(wf.getnframes())
        samples = np.frombuffer(data, dtype=np.int16)
        sd.play(samples, samplerate)
        sd.wait()

# def load_prompt(file_path):
#     """
#     Load the prompt from the file.
#     """
#     with open(file_path, "r") as f:
#         prompt = f.read()

#     return prompt

def convert_samplerate(file_path, new_rate=44100):
    sound = AudioSegment.from_wav(file_path)
    sound = sound.set_frame_rate(new_rate)
    new_file_path = file_path.stem + "_converted.wav"
    sound.export(new_file_path, format="wav")
    return new_file_path

def async_run(func, *args):
    thread = threading.Thread(target=func, args=args)
    thread.start()
    return thread


if __name__ == "__main__":
    # 会話履歴を格納するためのリストを初期化
    conversation_history = []

    # prompt = self.load_prompt("/root/HSR/chatgpt/baseline_questionnarie/prompt.txt")

    while True:
        # ユーザーに質問を入力させる
        input_prompt = input("質問を入力してください（終了するには'q'を入力）: ")

        # 終了条件の確認
        if input_prompt.lower() == 'q':
            break

        # GPT-4からの回答を生成
        # s_gpt = time.time()
        text = generate_text(input_prompt, conversation_history)
        # e_gpt = time.time()  # 終了時刻を取得
        # gpt_time = e_gpt - s_gpt
        # print(f"GPT prossing is {round(gpt_time, 2)}")

        # 回答を表示
        print("GPT-4からの回答:", text)

        # 音声合成 (VOICEBOX) # 日本語のみ
        # sound_synthesis_voicebox(text)
        # s_voice = time.time()
        # async_run(sound_synthesis_voicebox, text)
        # e_voice = time.time()  # 終了時刻を取得
        # voice_time = e_voice - s_voice
        # print(f"VOICEVOX prossing is {round(voice_time, 2)} seconds")

        # 音声合成 (Google Text-to-Speech) # 多言語OK
        sound_synthesis_gtts(text)
        # s_gtts = time.time()
        # async_run(sound_synthesis_gtts, text)
        # e_gtts = time.time()  # 終了時刻を取得
        # gtts_time = e_gtts - s_gtts
        # print(f"GTTS prossing is {round(gtts_time, 2)} seconds")


